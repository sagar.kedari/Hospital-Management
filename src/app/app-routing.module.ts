import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginPageComponent } from './custom-modules/auth/login-page/login-page.component';

const routes: Routes = [
  {path: '', component: LoginPageComponent},
  {path: 'admin', loadChildren:()=>import('./custom-modules/admin/admin.module').then(m=>m.AdminModule)},
  {path: 'doctor', loadChildren:()=>import('./custom-modules/doctor/doctor.module').then(m=>m.DoctorModule)},
  {path: 'nurse', loadChildren:()=>import('./custom-modules/nurse/nurse.module').then(m=>m.NurseModule)}  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
