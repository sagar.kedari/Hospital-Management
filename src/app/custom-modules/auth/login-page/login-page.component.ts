import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ILogin, ILoginForm } from 'src/app/interface/app.types';
import { AuthService } from '../services/auth.service';
import { Router, TitleStrategy } from '@angular/router';
@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.scss']
})
export class LoginPageComponent implements OnInit {
  loginForm!:FormGroup
  userList:ILogin[] = [];
  loginData!:ILoginForm;
  userData:ILogin[] = [];
  isLoading:boolean = false;
  userRole:string=''
  constructor(public fb:FormBuilder, private authService:AuthService, private router:Router) { }

  async ngOnInit() {
   
    console.log(this.userData);
  

    this.userList = await this.authService.getLoginDetails();
    console.log(this.userList);
        this.loginForm = this.fb.group({
      email: new FormControl('',[Validators.required, Validators.email]),
      password: new FormControl('', [Validators.required, Validators.minLength(6)])
    })
  }


  onSubmit(loginForm:FormGroup) {
    this.authService.loginUser(loginForm.value).subscribe((res)=>{
      console.log(res)
      if(res.data.role==="62cd75655853a5d2c643dbaa"){
        this.userRole = 'admin'      }
      else if(res.data.role==="62cd75655853a5d2c643dbab"){
        this.userRole = 'doctor'
      }else if(res.data.role==="62cd75655853a5d2c643dbac"){
        this.userRole = 'nurse'
      }
      this.router.navigate([`${this.userRole}`])
      console.log(res)
      localStorage.setItem('name', res.data.name)
      localStorage.setItem('token', res.data.token)
      localStorage.setItem('role', res.data.role)
      localStorage.setItem('id', res.data.userId)
    })
    

  //   this.authService.validateUser(loginForm.value);
  // this.loginData = {
  //   email: String(this.loginForm.value.email),
  //   password: String(this.loginForm.value.password),
  // };
  // this.router.navigate([`${localStorage.getItem('role')}`])
  // alert('done');
  
  }

}
