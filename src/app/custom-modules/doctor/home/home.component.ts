import { Component, OnInit } from '@angular/core';
import { DoctorService } from '../doctor.service';
import {MatTableDataSource} from '@angular/material/table';
import { IChangeUser, IUser } from 'src/app/interface/app.types';
import { ThisReceiver } from '@angular/compiler';
import { FormGroup, FormControl } from '@angular/forms';
import { Route, Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
id!:string
userData!:IUser[];
nurseList:any=[];
allNurseList:any=[];
showDialogBox=false;
columnsToBeDisplayed=['nurse','change-request']
dataSource = new MatTableDataSource<any>;
  constructor(private doctorService:DoctorService, private router:Router) { }

  ngOnInit(): void {
    
  }
  form=new FormGroup({
    for:new FormControl(''),
    reason:new FormControl(''),
    replacement: new FormControl('')
  })

  getNurses(){
    this.doctorService.getNurseList().subscribe((res)=>{
      console.log(res)
      this.userData = Object(res).data;
      console.log(this.userData)
      this.userData.filter(ele=>{
        if(ele.role==='62cd75655853a5d2c643dbac'){
          this.allNurseList.push(ele)
        }
      })
      console.log(this.allNurseList)
      this.id=localStorage.getItem('id')!
      this.userData.filter(ele=>{
        if(ele._id===`${this.id}`){
          console.log(ele.nurses)
          this.nurseList=ele.nurses
        }
        this.dataSource=this.nurseList
      })
      this.dataSource=this.nurseList
      console.log(this.dataSource)
    })
  }
  dialogBox(){
    this.showDialogBox=!this.showDialogBox
  }
  addRequest(form:FormGroup){
    form.get('role')?.setValue('62cd75655853a5d2c643dbac')
    console.log(form.value)
    form.get('for')?.setValue(`${this.id}`);
    this.doctorService.changeNurse(form.value).subscribe((res)=>{
      alert('Request Sent Successfully');
    })
    this.showDialogBox = !this.showDialogBox;
  }
  toChangeRequest(){
    this.router.navigate(['/doctor/change-request'])
  }

}
