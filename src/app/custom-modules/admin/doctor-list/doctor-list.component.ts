import { Component, OnInit } from '@angular/core';

import {MatTable} from '@angular/material/table'
import {AfterViewInit, ViewChild} from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';

import {MatTableDataSource} from '@angular/material/table';
import { AdminServiceService } from '../admin-service.service';
import { IDeleteUser, IDoctorList, IUser } from 'src/app/interface/app.types';
import { TitleStrategy } from '@angular/router';
import { FormControl, FormGroup } from '@angular/forms';



@Component({
  selector: 'app-doctor-list',
  templateUrl: './doctor-list.component.html',
  styleUrls: ['./doctor-list.component.scss']
})
export class DoctorListComponent implements AfterViewInit, OnInit {
  columnsToBeDisplayed: string[] = ['name', 'speciality', 'nurses','edit','delete'];
  dataSource = new MatTableDataSource<IUser>;
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild('table') myTable!:MatTable<any>;
  // form!:FormGroup
  form=new FormGroup({
    name:new FormControl(''),
    email:new FormControl(''),
    speciality: new FormControl(''),
    role: new FormControl('')
  })

  // dataSource: MatTableDataSource<UserData>;
  doctorList:any=[]
  nurseList:any=[]
  userData!:IUser[]
  showDialogBox=false;

  constructor(private adminService:AdminServiceService) { 
    
  }
  ngOnInit(){
    this.getUsers();
  }

  ngAfterViewInit(): void {
    this.dataSource.paginator = this.paginator;
    // this.dataSource.sort = this.sort;
  }
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            
  
  getUsers(){    
    this.adminService.getDoctorList().subscribe((res)=>{
      this.userData = Object(res).data;      
      this.userData.filter(ele=>{
        if(ele.role == '62cd75655853a5d2c643dbab'){
          this.doctorList.push(ele);
        }else if(ele.role == '62cd75655853a5d2c643dbac'){
          this.nurseList.push(ele);
        }
      })
      this.dataSource=this.doctorList
      console.log(this.dataSource)
      
    })
  }

  dialogBox(){
    this.showDialogBox=!this.showDialogBox
    return this.showDialogBox
  }
  displayNurseList(){
    this.dataSource=this.nurseList;
  }
  addUser(form:any){
    console.log(form)
    console.log(form.value)
    form.get('role')?.setValue('62cd75655853a5d2c643dbab')
    this.adminService.postData(form.value).subscribe((res)=>{
        alert('User added');
      this.getUsers();
      this.displayNurseList();
    })
  }

  editDoctor(){

  }

  deleteDoctor(element:IDeleteUser){
    console.log(element)
    console.log(element._id)
    this.adminService.deleteDoctorData(element._id).subscribe((res)=>{
      if(res){
        alert('Doctor deleted')
        console.log(res)
      }
      // this.getUsers()
    })
  }
  

}   
 
