import { Component, OnInit } from '@angular/core';
import {MatIconModule} from '@angular/material/icon';
import {MatTableDataSource} from '@angular/material/table';
import { IDoctorList, IUser } from 'src/app/interface/app.types';
import { NurseService } from '../nurse.service';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-nurse-home',
  templateUrl: './nurse-home.component.html',
  styleUrls: ['./nurse-home.component.scss']
})
export class NurseHomeComponent implements OnInit {
  showDialogBox:boolean=false
  dataSource=[{name:'nurse1'}];

  id:string=''
  showMessageBox:boolean=false;
  docData!:any;
  form!:FormGroup;
  assignedDoctorId:string='';
  assignedDoctorName:string='';

  nurseDetails:any;
  
  columnsToBeDisplayed=['name','change-request','send-msg']
  userData:any = [];
  constructor(private userService:NurseService) { }
  
  ngOnInit(): void {
    
    this.userService.getDoctorName().subscribe((res)=>{
      this.nurseDetails=Object(res).data;
      console.log('nurse user',res);
      this.userData = Object(res).data.assignedDoctor;
      console.log('doctor id',this.userData._id)
      this.assignedDoctorId=this.userData._id;
      this.assignedDoctorName=this.userData.name;
      
      // this.dataSource = this.userData
      // this.userData.assignedDoctor._id
     // console.log('doctor id',this.userData.assignedDoctor._id);
      

     
         
    })
    
    this.form = new FormGroup({
      text:new FormControl(''),
      from:new FormControl(this.id),
      for:new FormControl(this.assignedDoctorId)
      
    })
  }
  dialogBox(){
    this.showDialogBox=!this.showDialogBox
    return this.showDialogBox
  }
  
  openMessageBox(){
    this.showMessageBox=!this.showMessageBox
    return this.showMessageBox
  }

  clickMessageBox(){
    this.showMessageBox=!this.showMessageBox
    return this.showMessageBox
  }
  sendMessage(form:FormGroup){
    console.log(form.value)
    this.userService.postMessage(form.value).subscribe(res=>{
      console.log(res);
      alert('success')
    },
    err=>{
      alert('errors')
    })
  }

}
