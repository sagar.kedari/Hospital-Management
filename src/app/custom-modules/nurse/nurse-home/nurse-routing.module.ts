import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NurseHomeComponent } from './nurse-home.component';

const routes: Routes = [
  { path: '', component: NurseHomeComponent },
  { path: 'home', component: NurseHomeComponent },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class NurseRoutingModule {}
