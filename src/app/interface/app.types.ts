export interface ILogin {
  email: string;
  password: string;
  role: string;
}

export interface ILoginForm {
  email: string;
  password: string;
}
export interface IDoctorList {
  id: string;
  name: string;
  email: string;
  password: string;
  role: string;
}
export interface IUser {
  _id: string;
  name: string;
  email: string;
  password: string;
  role: string;
  assignedDoctor: [];
  speciality: null;
  nurses: [];
  resetToken: null;
  resetTokenExpiry: null;
  createdAt: string;
  updatedAt: string;
}
export interface IuserData{
          email:string;
          password:string
}
export interface IDoctor{
  id:string;
  name:string;
  speciality:string;
  nurses:[];  
}
export interface IDeleteUser{
  _id:string
}
export interface IChangeUser{
  reason:string;
  replacement:string;
  role:string
}
export interface INurse{
  for:string,
    replacement:string,
    reason:string

}

